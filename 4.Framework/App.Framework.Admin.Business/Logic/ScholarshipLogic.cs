﻿using App.Domain.Model;
using App.Framework.Business.ILogic;
using App.Framework.DAL.IQueryRepositoryUnit;
using App.Framework.DAL.IRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Framework.Business.Logic
{
    public class ScholarshipLogic : IScholarshipLogic
    {
        private readonly IScholarshipUnit _ScholarshipUnit;
        private readonly IScholarshipQueryUnit _ScholarshipQueryUnit;
        private bool disposed = false;

        public ScholarshipLogic(IScholarshipUnit _ScholarshipUnit, IScholarshipQueryUnit _ScholarshipQueryUnit)
        {
            this._ScholarshipUnit = _ScholarshipUnit;
            this._ScholarshipQueryUnit = _ScholarshipQueryUnit;
        }

        #region Scholarship
        public async Task AddScholarship(Scholarship _Scholarship)
        {
            try
            {
                await this._ScholarshipUnit.ScholarshipRepository.AddAsync(_Scholarship);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task DeleteScholarship(Scholarship _Scholarship)
        {
            try
            {
                await this._ScholarshipUnit.ScholarshipRepository.DeleteAsync(_Scholarship);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task UpdateScholarship(Scholarship _Scholarship)
        {
            try
            {
                await this._ScholarshipUnit.ScholarshipRepository.UpdateAsync(_Scholarship);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }
        #endregion

        #region Scholarship Queries
        public async Task<List<Scholarship>> GetAllScholarships()
        {
            try
            {
                List<Scholarship> _Scholarships = await this._ScholarshipQueryUnit.ScholarshipQueryRepository.GetAsync(null, null, null, null, null, true) as List<Scholarship>;
                return _Scholarships;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task<List<Scholarship>> GetScholarship(Expression<Func<Scholarship, bool>> _Expression)
        {
            try
            {
                List<Scholarship> _Scholarships = await this._ScholarshipQueryUnit.ScholarshipQueryRepository.GetAsync(_Expression, null, null, null, null, true) as List<Scholarship>;
                return _Scholarships;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task<Scholarship> GetScholarshipByID(long ID)
        {
            try
            {
                Scholarship _Scholarship = await this._ScholarshipQueryUnit.ScholarshipQueryRepository.GetByIDAsync(ID);
                return _Scholarship;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }
        #endregion

        public async Task BeginTransaction()
        {
            try
            {
                await this._ScholarshipUnit.UnitOfWork.BeginTransaction();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task EndTransaction()
        {
            try
            {
                await this._ScholarshipUnit.UnitOfWork.EndTransaction();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await this._ScholarshipUnit.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (this._ScholarshipUnit != null)
                            this._ScholarshipUnit.Dispose();
                        if (this._ScholarshipQueryUnit != null)
                            this._ScholarshipQueryUnit.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public class ScholershipFilters
    {
        public string SearchText { get; set; }
    }
}
