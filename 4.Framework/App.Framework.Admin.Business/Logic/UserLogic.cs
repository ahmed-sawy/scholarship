﻿using App.Domain.Model;
using App.Framework.Business.ILogic;
using App.Framework.DAL.IQueryRepositoryUnit;
using App.Framework.DAL.IRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Framework.Business.Logic
{
    public class UserLogic : IUserLogic
    {
        private readonly IUserUnit _UserUnit;
        private readonly IUserQueryUnit _UserQueryUnit;
        private bool disposed = false;

        public UserLogic(IUserUnit _UserUnit, IUserQueryUnit _UserQueryUnit)
        {
            this._UserQueryUnit = _UserQueryUnit;
            this._UserUnit = _UserUnit;
        }

        #region User
        public async Task AddUser(User _User)
        {
            try
            {
                await this._UserUnit.UserRepository.AddAsync(_User);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task DeleteUser(User _User)
        {
            try
            {
                await this._UserUnit.UserRepository.DeleteAsync(_User);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task UpdateUser(User _User)
        {
            try
            {
                await this._UserUnit.UserRepository.UpdateAsync(_User);
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }
        #endregion


        #region UserQuery
        public Task<List<User>> GetActiveUser()
        {
            throw new NotImplementedException();
        }

        public async Task<List<User>> GetAllUser()
        {
            try
            {
                List<User> lst = await this._UserQueryUnit.UserQueryRepository.GetAsync(null, null, null, null, null, true) as List<User>;
                return lst;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task<List<User>> GetUser(Expression<Func<User, bool>> _Expression)
        {
            try
            {
                List<User> Ruslt = await this._UserQueryUnit.UserQueryRepository.GetAsync(_Expression, null, null, null, null, true) as List<User>;
                return Ruslt;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task<User> GetUserByID(long ID)
        {
            try
            {
                User _User = await this._UserQueryUnit.UserQueryRepository.GetByIDAsync(ID);
                return _User;
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }
        #endregion

        public async Task BeginTransaction()
        {
            try
            {
                await this._UserUnit.UnitOfWork.BeginTransaction();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (this._UserUnit != null)
                            this._UserUnit.Dispose();
                        if (this._UserQueryUnit != null)
                            this._UserQueryUnit.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task EndTransaction()
        {
            try
            {
                await this._UserUnit.UnitOfWork.EndTransaction();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await this._UserUnit.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

    }
}
