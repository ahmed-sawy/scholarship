﻿using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Framework.Business.ILogic
{
    public interface IScholarshipLogic : IDisposable
	{
		Task<List<Scholarship>> GetAllScholarships();
		Task<Scholarship> GetScholarshipByID(long ID);
		Task<List<Scholarship>> GetScholarship(Expression<Func<Scholarship, bool>> _Expression);
		Task AddScholarship(Scholarship _Scholarship);
		Task UpdateScholarship(Scholarship _Scholarship);
		Task DeleteScholarship(Scholarship _Scholarship);
		Task SaveChanges();
		Task BeginTransaction();
		Task EndTransaction();
	}
}
