﻿using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Framework.Business.ILogic
{
    public interface IUserLogic : IDisposable
    {
		Task<List<User>> GetAllUser();
		Task<List<User>> GetActiveUser();
		Task<User> GetUserByID(long ID);
		Task<List<User>> GetUser(Expression<Func<User, bool>> _Expression);
		Task AddUser(User _User);
		Task UpdateUser(User _User);
		Task DeleteUser(User _User);
		Task SaveChanges();
		Task BeginTransaction();
		Task EndTransaction();
	}
}
