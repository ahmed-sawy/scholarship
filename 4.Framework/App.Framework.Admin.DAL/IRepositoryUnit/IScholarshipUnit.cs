﻿using App.Core.Repository;
using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.IRepositoryUnit
{
    public interface IScholarshipUnit : IDisposable
    {
        IGenericRepository<Scholarship> ScholarshipRepository { get; }
        IUnitOfWork<ScholarshipEntities> UnitOfWork { get; }
    }
}
