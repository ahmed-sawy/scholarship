﻿using App.Core.Repository;
using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.IRepositoryUnit
{
    public interface IUserUnit : IDisposable
    {
        IGenericRepository<User> UserRepository { get; }
        IUnitOfWork<ScholarshipEntities> UnitOfWork { get; }
    }
}
