﻿using App.Core.Repository;
using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.IQueryRepositoryUnit
{
    public interface IUserQueryUnit : IDisposable
    {
        IGenericQueryRepository<User, ScholarshipEntities> UserQueryRepository { get; }
    }
}
