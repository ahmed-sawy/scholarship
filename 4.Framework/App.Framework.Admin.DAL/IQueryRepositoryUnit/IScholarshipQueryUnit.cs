﻿using App.Core.Repository;
using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.IQueryRepositoryUnit
{
    public interface IScholarshipQueryUnit : IDisposable
    {
        IGenericQueryRepository<Scholarship, ScholarshipEntities> ScholarshipQueryRepository { get; }
    }
}
