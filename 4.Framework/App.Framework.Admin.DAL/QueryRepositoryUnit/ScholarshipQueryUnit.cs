﻿using App.Core.Repository;
using App.Domain.Model;
using App.Framework.DAL.IQueryRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.QueryRepositoryUnit
{
    public class ScholarshipQueryUnit : IScholarshipQueryUnit
    {
		private bool disposed = false;
		private readonly IGenericQueryRepository<Scholarship, ScholarshipEntities> _ScholarshipQueryRepository;

        public ScholarshipQueryUnit(IGenericQueryRepository<Scholarship, ScholarshipEntities> _ScholarshipQueryRepository)
        {
			this._ScholarshipQueryRepository = _ScholarshipQueryRepository;
        }


		public IGenericQueryRepository<Scholarship, ScholarshipEntities> ScholarshipQueryRepository
		{
			get
			{
				try
				{
					return this._ScholarshipQueryRepository;
				}
				catch (Exception ex)
				{
					Dispose();
					throw;
				}
			}
		}

		private void Dispose(bool disposing)
		{
			try
			{
				if (!this.disposed)
				{
					if (disposing)
					{
						if (_ScholarshipQueryRepository != null)
							_ScholarshipQueryRepository.Dispose();

					}
				}
				this.disposed = true;
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public virtual void Dispose()
		{
			try
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}
