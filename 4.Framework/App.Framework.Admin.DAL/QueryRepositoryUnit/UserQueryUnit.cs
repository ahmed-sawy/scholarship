﻿using App.Core.Repository;
using App.Domain.Model;
using App.Framework.DAL.IQueryRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.QueryRepositoryUnit
{
    public class UserQueryUnit : IUserQueryUnit
    {
        private bool disposed = false;
        private readonly IGenericQueryRepository<User, ScholarshipEntities> _UserQueryRepository;

        public UserQueryUnit(IGenericQueryRepository<User, ScholarshipEntities> _UserQueryRepository)
        {
            this._UserQueryRepository = _UserQueryRepository;
        }

        public IGenericQueryRepository<User, ScholarshipEntities> UserQueryRepository
        {
            get
            {
                try
                {
                    return this._UserQueryRepository;
                }
                catch (Exception ex)
                {
                    Dispose();
                    throw;
                }
            }
        }

		private void Dispose(bool disposing)
		{
			try
			{
				if (!this.disposed)
				{
					if (disposing)
					{
						if (this._UserQueryRepository != null)
							this._UserQueryRepository.Dispose();
					}
				}
				this.disposed = true;
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public virtual void Dispose()
		{
			try
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}
