﻿using App.Core.Repository;
using App.Domain.Model;
using App.Framework.DAL.IRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.RepositoryUnit
{
    public class UserUnit : IUserUnit
    {
        private bool disposed = false;
        private readonly IUnitOfWork<ScholarshipEntities> _UnitOfWork;
        private readonly IGenericRepository<User> _UserRepository;

        public UserUnit(IUnitOfWork<ScholarshipEntities> _UnitOfWork, IGenericRepository<User> _UserRepository)
        {
            this._UnitOfWork = _UnitOfWork;
            this._UserRepository = _UserRepository;
        }


        public IGenericRepository<User> UserRepository
        {
            get
            {
                try
                {
                    if (this._UserRepository.SetDbContext == null)
                        this._UserRepository.SetDbContext = _UnitOfWork.context;
                    return this._UserRepository;
                }
                catch (Exception ex)
                {
                    Dispose();
                    throw;
                }
            }
        }

        public IUnitOfWork<ScholarshipEntities> UnitOfWork
        {
            get
            {
                try
                {
                    return this._UnitOfWork;
                }
                catch (Exception ex)
                {
                    Dispose();
                    throw;
                }
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (this._UnitOfWork != null)
                            this._UnitOfWork.Dispose();
                        if (this._UserRepository != null)
                            this._UserRepository.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
