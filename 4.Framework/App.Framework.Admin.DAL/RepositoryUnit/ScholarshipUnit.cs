﻿using App.Core.Repository;
using App.Domain.Model;
using App.Framework.DAL.IRepositoryUnit;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework.DAL.RepositoryUnit
{
    public class ScholarshipUnit : IScholarshipUnit
    {
        private bool disposed = false;
        private readonly IUnitOfWork<ScholarshipEntities> _UnitOfWork;
        private readonly IGenericRepository<Scholarship> _ScholarshipRepository;

        public ScholarshipUnit(IUnitOfWork<ScholarshipEntities> _UnitOfWork, IGenericRepository<Scholarship> _ScholarshipRepository)
        {
            this._UnitOfWork = _UnitOfWork;
            this._ScholarshipRepository = _ScholarshipRepository;
        }


        public IGenericRepository<Scholarship> ScholarshipRepository
        {
            get
            {
                try
                {
                    if (this._ScholarshipRepository.SetDbContext == null)
                        _ScholarshipRepository.SetDbContext = _UnitOfWork.context;
                    return this._ScholarshipRepository;
                }
                catch (Exception ex)
                {
                    Dispose();
                    throw;
                }
            }
        }

        public IUnitOfWork<ScholarshipEntities> UnitOfWork
        {
            get
            {
                try
                {
                    return this._UnitOfWork;
                }
                catch (Exception ex)
                {
                    Dispose();
                    throw;
                }
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (this._UnitOfWork != null)
                            this._UnitOfWork.Dispose();
                        if (this._ScholarshipRepository != null)
                            this._ScholarshipRepository.Dispose();

                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
