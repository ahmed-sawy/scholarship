﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace App.Core.Helpers
{
    public class CoreHelper
    {
        public static DateTime DateFormate(DateTime _DateTime)
        {
            try
            {
                string DateFormat = "MM/dd/yyyy";

                return _DateTime;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static TimeSpan TimeFormate(DateTime _DateTime)
        {
            try
            {
                return _DateTime.TimeOfDay;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static TimeSpan TimeFormate(TimeSpan _Time)
        {
            try
            {
                string TimeFormat = "";

                return _Time;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static DateTime AppendTimeToDate(DateTime date)
        {
            try
            {
                return new DateTime(date.Year, date.Month, date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static string PhoneReFormat(string _Phone)
        {
            try
            {
                string rExp = @"[^\w\d]";
                _Phone = Regex.Replace(_Phone, rExp, "");
                return _Phone;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string MobileReFormat(string _mobile)
        {
            try
            {
                string rExp = @"[^\w\d]";
                _mobile = Regex.Replace(_mobile, rExp, "");
                return _mobile;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string MobileFormate(string _Mobile)
        {
            try
            {
                string MobileFormat = "";

                return _Mobile;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static decimal DecimalFormate(decimal _Decimal)
        {
            try
            {
                string DecimalFormat = "";

                return _Decimal;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetEnumDescription(Enum value)
        {
            try
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;
                else
                    return value.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<string> GetErrorMessage(Exception exception)
        {
            try
            {
                List<string> ErrorMessage = new List<string>();

                if (exception.GetBaseException().Message == CoreConst.CustomeExceptionClassName)
                {
                    if (exception.GetBaseException().Data.Count > 0)
                    {
                        for (int i = 0; i < exception.GetBaseException().Data.Count; i++)
                        {
                            ErrorMessage.Add(exception.GetBaseException().Data[i + 1].ToString());
                        }
                    }
                }

                return ErrorMessage;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string Encrypt(string message, string KeyString, string IVString)
        {
            byte[] Key = ASCIIEncoding.UTF8.GetBytes(KeyString);
            byte[] IV = ASCIIEncoding.UTF8.GetBytes(IVString);

            string encrypted = null;
            RijndaelManaged rj = new RijndaelManaged
            {
                Key = Key,
                IV = IV,
                Mode = CipherMode.CBC
            };

            try
            {
                MemoryStream ms = new MemoryStream();

                using (CryptoStream cs = new CryptoStream(ms, rj.CreateEncryptor(Key, IV), CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        sw.Write(message);
                        sw.Close();
                    }
                    cs.Close();
                }
                byte[] encoded = ms.ToArray();
                encrypted = Convert.ToBase64String(encoded);

                ms.Close();
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("A file error occurred: {0}", e.Message);
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                rj.Clear();
            }
            return encrypted;
        }

        public static string Decrypt(string cipherData, string keyString, string ivString)
        {
            byte[] key = Encoding.UTF8.GetBytes(keyString);
            byte[] iv = Encoding.UTF8.GetBytes(ivString);

            try
            {
                using (var rijndaelManaged =
                       new RijndaelManaged { Key = key, IV = iv, Mode = CipherMode.CBC })
                using (var memoryStream =
                       new MemoryStream(Convert.FromBase64String(cipherData)))
                using (var cryptoStream =
                       new CryptoStream(memoryStream,
                           rijndaelManaged.CreateDecryptor(key, iv),
                           CryptoStreamMode.Read))
                {
                    return new StreamReader(cryptoStream).ReadToEnd();
                }
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
            // You may want to catch more exceptions here...
        }

        public static bool SendEmail(int Port, string Host, string ServerEmail, string Password, string To, string Subject, string Body, List<string> AttachmentFilePath)
        {
            using (var mail = new MailMessage(ServerEmail, To))
            {
                try
                {
                    mail.Subject = Subject;
                    mail.Body = Body;

                    if (AttachmentFilePath != null)
                    {
                        if (AttachmentFilePath.Count > 0)
                        {
                            foreach (string item in AttachmentFilePath)
                            {
                                Attachment attachment = new Attachment(item, MediaTypeNames.Application.Octet);
                                ContentDisposition disposition = attachment.ContentDisposition;
                                disposition.CreationDate = File.GetCreationTime(item);
                                disposition.ModificationDate = File.GetLastWriteTime(item);
                                disposition.ReadDate = File.GetLastAccessTime(item);
                                disposition.FileName = Path.GetFileName(item);
                                disposition.Size = new FileInfo(item).Length;
                                disposition.DispositionType = DispositionTypeNames.Attachment;
                                mail.Attachments.Add(attachment);
                            }
                        }
                    }

                    using (SmtpClient client = new SmtpClient())
                    {
                        NetworkCredential credentials = new NetworkCredential(ServerEmail, Password);
                        client.Host = Host;
                        //client.Port = Port;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.EnableSsl = false;
                        client.UseDefaultCredentials = false;
                        client.Credentials = credentials;
                        client.Timeout = 600000;
                        client.Send(mail);
                        client.Dispose();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public async Task SendGmailMessage(int Port, string Host, string SenderEmail, string SenderDisplayName, string SenderEmailPassword, string ReceiverEmail, string EmaiSubject, string EmailBody)
        {
            try
            {
                var fromAddress = new MailAddress(SenderEmail, SenderDisplayName);
                var toAddress = new MailAddress(ReceiverEmail, ReceiverEmail);
                string fromPassword = SenderEmailPassword;
                string subject = EmaiSubject;
                string body = EmailBody;

                var smtp = new SmtpClient
                {
                    Host = Host,
                    //Port = Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                //return true;
            }
            catch (Exception ex)
            {
                //return false;
            }
        }

    }
}
