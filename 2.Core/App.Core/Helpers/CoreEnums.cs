﻿using System.ComponentModel;

namespace App.Core.Helpers
{
    public static class CoreEnums
    {
        public enum OrderDirection
        {
            Ascending = 1,
            Descending = 2,
        };

        public enum SignInStatus
        {
            Success = 1,
            UserNotFound = 2,
            AccountInActive = 3,
            AccountExpired = 4
        };

        public enum Lang
        {
            EN = 1,
            AR = 2
        };

        public enum Confirmation
        {
            Yes = 1,
            No = 2
        };
    }
}
