﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Core.Repository
{
    public class GenericQueryRepository<T, C> : IGenericQueryRepository<T, C> where T : class where C : DbContext, new()
    {
        private bool disposed = false;
        protected DbSet<T> dbSet;

        public GenericQueryRepository()
        {
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.ProxyCreationEnabled = false;
            dbSet = context.Set<T>();
        }

        private C _entities = new C();
        public C context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false)
        {
            try
            {
                includeProperties = includeProperties ?? string.Empty;
                IQueryable<T> query = dbSet;

                if (filter != null)
                    query = query.Where(filter);

                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                if (orderBy != null)
                    query = orderBy(query);

                if (skip.HasValue)
                    query = query.Skip(skip.Value);

                if (take.HasValue)
                    query = query.Take(take.Value);

                if (NoTracking)
                    query = query.AsNoTracking();

                return query;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual T GetOne(Expression<Func<T, bool>> filter = null, string includeProperties = "", bool NoTracking = false)
        {
            try
            {
                return GetQueryable(filter, null, includeProperties, null, null, NoTracking).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<T> GetOneAsync(Expression<Func<T, bool>> filter = null, string includeProperties = null, bool NoTracking = false)
        {
            try
            {
                return await GetQueryable(filter, null, includeProperties, null, null, NoTracking).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual T GetFirst(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool NoTracking = false)
        {
            try
            {
                return GetQueryable(filter, orderBy, includeProperties, null, null, NoTracking).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<T> GetFirstAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, bool NoTracking = false)
        {
            try
            {
                return await GetQueryable(filter, orderBy, includeProperties, null, null, NoTracking).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual int GetCount(Expression<Func<T, bool>> filter = null)
        {
            try
            {
                return GetQueryable(filter).Count();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual Task<int> GetCountAsync(Expression<Func<T, bool>> filter = null)
        {
            try
            {
                return GetQueryable(filter).CountAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual bool GetExists(Expression<Func<T, bool>> filter = null)
        {
            try
            {
                return GetQueryable(filter).Any();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual Task<bool> GetExistsAsync(Expression<Func<T, bool>> filter = null)
        {
            try
            {
                return GetQueryable(filter).AnyAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual IEnumerable<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false)
        {
            try
            {
                return GetQueryable(null, orderBy, includeProperties, take, skip, NoTracking).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false)
        {
            try
            {
                return await GetQueryable(null, orderBy, includeProperties, take, skip, NoTracking).ToListAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual T GetByID(object id)
        {
            try
            {
                return dbSet.Find(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<T> GetByIDAsync(object id)
        {
            try
            {
                return await dbSet.FindAsync(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", int? take = null, int? skip = null, bool asNoTracking = false)
        {
            try
            {
                return GetQueryable(filter, orderBy, includeProperties, take, skip, asNoTracking).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", int? take = null, int? skip = null, bool asNoTracking = false)
        {
            try
            {
                return await GetQueryable(filter, orderBy, includeProperties, take, skip, asNoTracking).ToListAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual DbRawSqlQuery<T> SQLQuery(string sql, params object[] parameters)
        {
            try
            {
                return context.Database.SqlQuery<T>(sql, parameters);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (context != null)
                            context.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
