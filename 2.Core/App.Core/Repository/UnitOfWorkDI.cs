﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace App.Core.Repository
{
    public class UnitOfWorkDI<C> : IUnitOfWork<C> where C : DbContext, new()
    {
        private DbContextTransaction Transaction;
        private bool disposed = false;

        public UnitOfWorkDI()
        {

        }

        private C _entities = new C();
        public C context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public void SaveChanges()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task BeginTransaction()
        {
            try
            {
                await Task.Run(() =>
                {
                    Transaction = context.Database.BeginTransaction();
                });
                
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        public async Task EndTransaction()
        {
            try
            {
                await Task.Run(() =>
                {
                    Transaction.Commit();
                });
            }
            catch (Exception ex)
            {
                TransactionRollBack();
                Dispose();
                throw;
            }
        }

        private void TransactionRollBack()
        {
            try
            {
                Transaction.Rollback();
            }
            catch (Exception ex)
            {
                Dispose();
                throw;
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (Transaction != null)
                            Transaction.Dispose();

                        if (context != null)
                            context.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
