﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace App.Core.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private bool disposed = false;
        protected DbContext context;
        protected DbSet<T> dbSet;

        public GenericRepository()
        {
            
        }

        public DbContext SetDbContext
        {
            set
            {
                context = value;
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                dbSet = context.Set<T>();
            }
            get
            {
                return context;
            }
        }

        public void Add(T entity)
        {
            try
            {
                context.Entry(entity).State = EntityState.Added;
                dbSet.Add(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Add(IEnumerable<T> entities)
        {
            try
            {
                foreach (var Obj in entities)
                {
                    Add(Obj);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task AddAsync(T entity)
        {
            try
            {
                Add(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task AddAsync(IEnumerable<T> entities)
        {
            try
            {
                foreach (var Obj in entities)
                {
                    await AddAsync(Obj);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Update(T entity)
        {
            try
            {
                if (context.Entry(entity).State == EntityState.Detached)
                    dbSet.Attach(entity);

                var entry = context.Entry(entity);
                var databaseValues = entry.GetDatabaseValues();

                foreach (var propertyName in databaseValues.PropertyNames)
                {
                    var original = databaseValues.GetValue<object>(propertyName);
                    var current = entry.CurrentValues.GetValue<object>(propertyName);

                    if (Nullable.GetUnderlyingType(entity.GetType().GetProperty(propertyName).PropertyType) == null)
                    {
                        if (current is DateTime)
                        {
                            if ((DateTime)current == new DateTime())
                                continue;
                        }

                        if (current is int)
                        {
                            if ((int)current == new int())
                                continue;
                        }

                        if (current is long)
                        {
                            if ((long)current == new long())
                                continue;
                        }

                        if (current is bool)
                        {
                            if ((bool?)current == null)
                                continue;
                        }
                    }

                    if ((original != null && !original.Equals(current)) || (current != null && !current.Equals(original)))
                        entry.Property(propertyName).IsModified = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task UpdateAsync(T entity)
        {
            try
            {
                Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Delete(object id)
        {
            T entity = dbSet.Find(id);
            Delete(entity);
        }

        public void Delete(T entity)
        {
            try
            {
                if (context.Entry(entity).State == EntityState.Detached || context.Entry(entity).State == EntityState.Added)
                    dbSet.Attach(entity);

                dbSet.Remove(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task DeleteAsync(T entity)
        {
            try
            {
                Delete(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task DeleteAsync(object id)
        {
            try
            {
                Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (context != null)
                            context.Dispose();
                    }
                }
                this.disposed = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
