﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Repository
{
    public interface IGenericQueryRepository<T, C> : IDisposable where T : class where C : DbContext, new()
    {
        C context { get; set; }
        IQueryable<T> GetQueryable(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false);
        T GetOne(Expression<Func<T, bool>> filter = null, string includeProperties = "", bool NoTracking = false);
        Task<T> GetOneAsync(Expression<Func<T, bool>> filter = null, string includeProperties = null, bool NoTracking = false);
        T GetFirst(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool NoTracking = false);
        Task<T> GetFirstAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, bool NoTracking = false);
        int GetCount(Expression<Func<T, bool>> filter = null);
        Task<int> GetCountAsync(Expression<Func<T, bool>> filter = null);
        bool GetExists(Expression<Func<T, bool>> filter = null);
        Task<bool> GetExistsAsync(Expression<Func<T, bool>> filter = null);
        IEnumerable<T> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false);
        Task<IEnumerable<T>> GetAllAsync(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? take = null, int? skip = null, bool NoTracking = false);
        T GetByID(object id);
        Task<T> GetByIDAsync(object id);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", int? take = null, int? skip = null, bool asNoTracking = false);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", int? take = null, int? skip = null, bool asNoTracking = false);
        DbRawSqlQuery<T> SQLQuery(string sql, params object[] parameters);
    }
}
