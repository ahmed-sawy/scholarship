﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace App.Core.Repository
{
    public interface IUnitOfWork<C> : IDisposable where C : DbContext, new()
    {
        C context { get; set; }
        void SaveChanges();
        Task SaveChangesAsync();
        Task BeginTransaction();
        Task EndTransaction();
    }
}
