﻿using App.Core.Helpers;
using System;
using System.Collections.Generic;

namespace App.Core.ExceptionHandle
{
    public class CustomException : Exception
    {
        public CustomException(int Index, string MessageValue) : base(CoreConst.CustomeExceptionClassName)
        {
            try
            {
                base.Data.Clear();
                base.Data.Add(Index, MessageValue);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public CustomException(Dictionary<int, string> MessageValues) : base(CoreConst.CustomeExceptionClassName)
        {
            try
            {
                base.Data.Clear();
                foreach (KeyValuePair<int, string> entry in MessageValues)
                {
                    base.Data.Add(entry.Key, entry.Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
