﻿using log4net;
using log4net.Config;
using System;

namespace App.Core.Log
{
    public class AppLogHelper
    {
        private static ILog _Logger;

        public static ILog GetLogger(Type type)
        {
            try
            {
                if (_Logger != null)
                    return _Logger;

                XmlConfigurator.Configure();
                _Logger = LogManager.GetLogger(type);

                return _Logger;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
