﻿
namespace App.Core.QueryBase
{
    public class PagingFiltersBase : OrderFiltersBase
    {
        public int PageNumber { get; set; }
        public int? Take { get; set; }
        public int? Skip
        {
            get
            {
                return (PageNumber - 1) * Take;
            }
        }

        public int TotalCount { get; set; }
        //public int StartPage { get; set; }
        //public int EndPage { get; set; }
    }
}
