﻿using App.Core.Helpers;

namespace App.Core.QueryBase
{
    public class OrderFiltersBase
    {
        public string OrderColumn { get; set; }
        public CoreEnums.OrderDirection OrderDirection { get; set; } = CoreEnums.OrderDirection.Ascending;
    }
}
