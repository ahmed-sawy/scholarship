﻿using App.Core.Helpers;
using App.Resources;
using App.UI.WEB.ASP.MVC.IBase;
using App.UI.WEB.ASP.MVC.Models;
using App.UI.WEB.ASP.MVC.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.UI.WEB.ASP.MVC.Helpers;

namespace App.UI.WEB.ASP.MVC.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly IAuthenticationUser _AuthenticationUser;

        public LoginController(IAuthenticationUser AuthenticationUser)
        {
            _AuthenticationUser = AuthenticationUser;
        }

        public ActionResult Index()
        {
            _AuthenticationUser.SignOut();
            return View(new LoginModel());
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> SignIn(LoginModel _LoginModel)
        {
            try
            {
                _AuthenticationUser.SignOut();

                if (!ModelState.IsValid)
                    return View("Index", _LoginModel);

                var SignInStatus = await _AuthenticationUser.SignIn(_LoginModel.username, _LoginModel.pass);

                if (SignInStatus == CoreEnums.SignInStatus.UserNotFound)
                {
                    ModelState.AddModelError("", AppResources.UserNotFound);
                    return View("Index", _LoginModel);
                }

                if (SignInStatus == CoreEnums.SignInStatus.AccountInActive)
                {
                    ModelState.AddModelError("", AppResources.AccountInActive);
                    return View("Index", _LoginModel);
                }

                if (SignInStatus == CoreEnums.SignInStatus.AccountExpired)
                {
                    ModelState.AddModelError("", AppResources.AccountExpired);
                    return View("Index", _LoginModel);
                }

                byte AuthenticationRoleID = 0;
                byte.TryParse(_AuthenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

                return RedirectToAction("Index", "Scholership");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}