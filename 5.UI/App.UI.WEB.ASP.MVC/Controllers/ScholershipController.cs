﻿using App.Core.Helpers;
using App.Domain.Model;
using App.Framework.Business.ILogic;
using App.Framework.Business.Logic;
using App.UI.WEB.ASP.MVC.Base;
using App.UI.WEB.ASP.MVC.Helpers;
using App.UI.WEB.ASP.MVC.IBase;
using App.UI.WEB.ASP.MVC.Models;
using AutoMapper;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace App.UI.WEB.ASP.MVC.Controllers
{
    public class ScholershipController : BaseController
    {
        private readonly IAuthenticationUser authenticationUser;
        private readonly IScholarshipLogic scholarshipLogic;

        public ScholershipController(IAuthenticationUser authenticationUser, IScholarshipLogic scholarshipLogic)
        {
            this.authenticationUser = authenticationUser;
            this.scholarshipLogic = scholarshipLogic;
        }

        // GET: Scholership
        public async Task<ActionResult> Index()
        {
            ScholarshipListModel scholershipList = new ScholarshipListModel();
            scholershipList.Scholerships = new List<ScholarshipModel>();
            scholershipList.ScholershipFilters = new ScholershipFilters();
            scholershipList.ScholershipFilters.SearchText = "";

            try
            {
                byte AuthenticationRoleID = 0;
                byte.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

                ViewBag.AuthenticationRoleID = AuthenticationRoleID;

                List<Scholarship> scholarships = new List<Scholarship>();

                if (AuthenticationRoleID == (int)SecurityEnums.Role.Admin)
                {
                    scholarships = await this.scholarshipLogic.GetAllScholarships();
                }
                else if(AuthenticationRoleID == (int)SecurityEnums.Role.Graduate)
                {
                    long LoginUser_ID = 0;
                    long.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.ID).ToString(), out LoginUser_ID);

                    scholarships = await this.scholarshipLogic.GetScholarship(s => s.UserID == LoginUser_ID);
                }

                if (scholarships != null)
                {
                    scholershipList.Scholerships = Mapper.Map<List<Scholarship>, List<ScholarshipModel>>(scholarships);
                }

                return View(scholershipList);
            }
            catch (Exception ex)
            {
                List<string> ErrorMessage = CoreHelper.GetErrorMessage(ex);

                if (ErrorMessage.Any())
                {
                    Parallel.ForEach(ErrorMessage, ErrorMessageObj =>
                    {
                        ModelState.AddModelError("", ErrorMessageObj);
                    });

                    return View(scholershipList);
                }
                else
                {
                    throw;
                }
            }
        }


        [HttpGet]
        public async Task<ActionResult> Create(long? id)
        {
            ScholarshipModel scholarshipModel = new ScholarshipModel();

            try
            {
                if (id.HasValue)
                {
                    Scholarship scholarship = await this.scholarshipLogic.GetScholarshipByID(id.Value);

                    if (scholarship != null)
                        scholarshipModel = Mapper.Map<Scholarship, ScholarshipModel>(scholarship, scholarshipModel);
                }

                ViewBag.Create = true;
                ModelState.Clear();

                return PartialView(scholarshipModel);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Save(ScholarshipModel scholarshipModel)
        {
            try
            {
                ViewBag.SaveSuccess = false;
                TempData["Save"] = false;

                if (ModelState.IsValid)
                {
                    long LoginUser_ID = 0;
                    long.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.ID).ToString(), out LoginUser_ID);

                    //if (merchantModel.inputFile == null && TempData["inputFile"] != null)
                    //    merchantModel.inputFile = (HttpPostedFileBase)TempData.Peek("inputFile");

                    //if (merchantModel.inputFile != null)
                    //{
                    //    string SpecificPath = "Merchants/";
                    //    merchantModel.LogoPath = WebAppHelper.SaveFile(merchantModel.inputFile, AppConstant.FileType.Image, SpecificPath);
                    //    TempData["inputFile"] = merchantModel.inputFile;
                    //}

                    //if (!string.IsNullOrEmpty(merchantModel.LogoPath))
                    //    merchantModel.ImagePreview = WebAppHelper.GetAppSettingsValueByKey(AppConstant.AppSettingsKeys.FileSystemPath) + merchantModel.LogoPath;
                    //else
                    //    merchantModel.ImagePreview = "~/web-assets/images/noImage.png";

                    if (scholarshipModel.ID == 0)
                    {
                        Scholarship scholarship = Mapper.Map<ScholarshipModel, Scholarship>(scholarshipModel);

                        //product.Abbreviation = _MenuLanguage.Abbreviation[0].ToString().ToUpper() + _MenuLanguage.Abbreviation[1].ToString().ToLower();

                        scholarship.UserID = LoginUser_ID;
                        scholarship.Resume = "";

                        await this.scholarshipLogic.AddScholarship(scholarship);
                        await this.scholarshipLogic.SaveChanges();

                        scholarshipModel = new ScholarshipModel();

                    }
                    else
                    {
                        Scholarship scholarship = await this.scholarshipLogic.GetScholarshipByID(scholarshipModel.ID);
                        //string _LogoPath = merchant.LogoPath;

                        scholarship = Mapper.Map<ScholarshipModel, Scholarship>(scholarshipModel, scholarship);

                        await this.scholarshipLogic.UpdateScholarship(scholarship);
                        await this.scholarshipLogic.SaveChanges();

                        //if (_LogoPath != null && merchantModel.inputFile != null)
                        //    WebAppHelper.DeleteFile(_LogoPath);

                        //if (_LogoPath != null && string.IsNullOrEmpty(merchant.LogoPath))
                        //    WebAppHelper.DeleteFile(_LogoPath);

                        scholarshipModel = Mapper.Map<Scholarship, ScholarshipModel>(scholarship, scholarshipModel);
                    }

                    ModelState.Clear();

                    ViewBag.SaveSuccess = true;
                    TempData["Save"] = true;
                }

                return PartialView("Create", scholarshipModel);
            }
            catch (Exception ex)
            {
                List<string> ErrorMessage = CoreHelper.GetErrorMessage(ex);
                if (ErrorMessage.Any())
                {
                    Parallel.ForEach(ErrorMessage, ErrorMessageObj =>
                    {
                        ModelState.AddModelError("", ErrorMessageObj);
                    });

                    ViewBag.SaveSuccess = false;

                    return PartialView("Create", scholarshipModel);
                }
                else
                {
                    throw;
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ListItemActions(ScholarshipModel scholarshipModel, string ActionName)
        {
            try
            {
                Scholarship scholarship = await this.scholarshipLogic.GetScholarshipByID(scholarshipModel.ID);

                if (scholarship != null)
                {
                    if (ActionName == "Delete")
                    {
                        await this.scholarshipLogic.DeleteScholarship(scholarship);
                        await this.scholarshipLogic.SaveChanges();
                        ViewBag.DeleteSuccess = true;
                        TempData["Delete"] = true;
                    }
                    else
                    {
                        if (ActionName == "Edit")
                        {
                            if (!ModelState.IsValid)
                                return PartialView("ItemList", scholarshipModel);

                            scholarship = Mapper.Map<ScholarshipModel, Scholarship>(scholarshipModel, scholarship);

                            ViewBag.SaveSuccess = true;
                        }

                        if(ActionName == "Approve")
                        {
                            scholarship.IsApproved = true;
                            ViewBag.SaveSuccess = true;
                        }
                        
                        if(ActionName == "Disapprove")
                        {
                            scholarship.IsApproved = false;
                            ViewBag.SaveSuccess = true;
                        }

                        byte AuthenticationRoleID = 0;
                        byte.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

                        ViewBag.AuthenticationRoleID = AuthenticationRoleID;

                        await this.scholarshipLogic.UpdateScholarship(scholarship);
                        await this.scholarshipLogic.SaveChanges();

                        ModelState.Clear();

                        scholarshipModel = Mapper.Map<Scholarship, ScholarshipModel>(scholarship, scholarshipModel);
                    }
                }
                else
                {
                    ViewBag.DataNotFound = true;
                }

                return PartialView("ItemList", scholarshipModel);
            }
            catch (Exception ex)
            {
                List<string> ErrorMessage = CoreHelper.GetErrorMessage(ex);
                if (ErrorMessage.Any())
                {
                    Parallel.ForEach(ErrorMessage, ErrorMessageObj =>
                    {
                        ModelState.AddModelError("", ErrorMessageObj);
                    });
                    ViewBag.SaveSuccess = false;
                    return PartialView("ItemList", scholarshipModel);
                }
                else
                {
                    throw;
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Search(ScholarshipListModel scholarshipList, string ActionName)
        {
            try
            {
                byte AuthenticationRoleID = 0;
                byte.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

                List<Scholarship> scholarships = new List<Scholarship>();

                if (AuthenticationRoleID == (int)SecurityEnums.Role.Admin)
                {
                    scholarships = await this.scholarshipLogic.GetAllScholarships();
                }
                else if (AuthenticationRoleID == (int)SecurityEnums.Role.Graduate)
                {
                    long LoginUser_ID = 0;
                    long.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.ID).ToString(), out LoginUser_ID);

                    scholarships = await this.scholarshipLogic.GetScholarship(s => s.UserID == LoginUser_ID);
                }

                if (scholarshipList == null)
                {
                    scholarshipList = new ScholarshipListModel();
                    scholarshipList.ScholershipFilters.SearchText = "";
                }

                if (ActionName == "Refresh")
                    scholarshipList.ScholershipFilters.SearchText = "";

                scholarshipList.Scholerships = Mapper.Map<List<Scholarship>, List<ScholarshipModel>>(scholarships);

                ModelState.Clear();

                ViewBag.Search = true;

                return PartialView("List", scholarshipList);
            }
            catch (Exception ex)
            {
                List<string> ErrorMessage = CoreHelper.GetErrorMessage(ex);
                if (ErrorMessage.Any())
                {
                    Parallel.ForEach(ErrorMessage, ErrorMessageObj =>
                    {
                        ModelState.AddModelError("", ErrorMessageObj);
                    });
                    return View(scholarshipList);
                }
                else
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public async Task<ActionResult> DownloadExcel()
        {
            ScholarshipListModel scholershipList = new ScholarshipListModel();
            scholershipList.Scholerships = new List<ScholarshipModel>();
            scholershipList.ScholershipFilters = new ScholershipFilters();
            scholershipList.ScholershipFilters.SearchText = "";


            byte AuthenticationRoleID = 0;
            byte.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

            if (AuthenticationRoleID == (int)SecurityEnums.Role.Admin)
            {
                scholershipList.Scholerships = Mapper.Map<List<Scholarship>, List<ScholarshipModel>>(await this.scholarshipLogic.GetAllScholarships());
            }
            else if (AuthenticationRoleID == (int)SecurityEnums.Role.Graduate)
            {
                long LoginUser_ID = 0;
                long.TryParse(this.authenticationUser.GetUserProfile(Base.AppConstant.UserPorfile.ID).ToString(), out LoginUser_ID);

                scholershipList.Scholerships = Mapper.Map<List<Scholarship>, List<ScholarshipModel>>(await this.scholarshipLogic.GetScholarship(s => s.UserID == LoginUser_ID));
            }


            var gv = new GridView();
            gv.DataSource = scholershipList.Scholerships;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();

            return View(scholershipList);
        }
    }
}