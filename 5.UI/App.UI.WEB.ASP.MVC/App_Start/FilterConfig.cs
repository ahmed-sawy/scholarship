﻿using App.UI.WEB.ASP.MVC.Base;
using System.Web;
using System.Web.Mvc;

namespace App.UI.WEB.ASP.MVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionFilter() { ExceptionType = typeof(HttpAntiForgeryException) });
        }
    }
}
