using App.Core.Repository;
using App.Framework.Business.ILogic;
using App.Framework.Business.Logic;
using App.Framework.DAL.IQueryRepositoryUnit;
using App.Framework.DAL.IRepositoryUnit;
using App.Framework.DAL.QueryRepositoryUnit;
using App.Framework.DAL.RepositoryUnit;
using App.UI.WEB.ASP.MVC.Base;
using App.UI.WEB.ASP.MVC.IBase;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace App.UI.WEB.ASP.MVC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            container.RegisterType(typeof(IGenericQueryRepository<,>), typeof(GenericQueryRepository<,>));
            container.RegisterType(typeof(IUnitOfWork<>), typeof(UnitOfWorkDI<>));

            container.RegisterType<IAuthenticationUser, AuthenticationUser>();

            container.RegisterType<IUserUnit, UserUnit>();
            container.RegisterType<IUserQueryUnit, UserQueryUnit>();
            container.RegisterType<IUserLogic, UserLogic>();

            container.RegisterType<IScholarshipUnit, ScholarshipUnit>();
            container.RegisterType<IScholarshipQueryUnit, ScholarshipQueryUnit>();
            container.RegisterType<IScholarshipLogic, ScholarshipLogic>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}