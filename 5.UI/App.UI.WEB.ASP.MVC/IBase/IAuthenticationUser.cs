﻿using App.Core.Helpers;
using App.UI.WEB.ASP.MVC.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace App.UI.WEB.ASP.MVC.IBase
{
    public interface IAuthenticationUser
    {
        Task<CoreEnums.SignInStatus> SignIn(string UserName, string password);

        void SignOut();

        object GetUserProfile(AppConstant.UserPorfile UserPorfileProperty);
    }
}