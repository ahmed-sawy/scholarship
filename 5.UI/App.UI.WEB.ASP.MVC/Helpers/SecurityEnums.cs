﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Helpers
{
    public static class SecurityEnums
    {
        public enum Role
        {
            Admin = 1,
            Graduate = 2
        };
    }
}