﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Base
{
    public static class WebAppHelper
    {
        public const string ImageExtensions = "jpg,png,jpeg,gif,bmp,tif,tiff,JPE,Jfif,DIB,JIF,HEIC";
        public const int LandingImageSize = 1000;
        public const int ImageSize = 250;

        public static void Setcookie(string Key, string Value)
        {
            try
            {
                var cookie = new HttpCookie(Key, Value)
                {
                    Expires = DateTime.Now.AddDays(1)
                };

                System.Web.HttpContext.Current.Response.Cookies.Remove(AppConstant.Lang);
                System.Web.HttpContext.Current.Response.SetCookie(cookie);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static string GetAppSettingsValueByKey(AppConstant.AppSettingsKeys _AppSettingsKeys)
        {
            try
            {
                string _value = "";

                if (_AppSettingsKeys == AppConstant.AppSettingsKeys.FileSystemPath)
                    _value = ConfigurationManager.AppSettings["FileSystemPath"];

                if (_AppSettingsKeys == AppConstant.AppSettingsKeys.EncryptionKey)
                    _value = ConfigurationManager.AppSettings["EncryptionKey"];

                if (_AppSettingsKeys == AppConstant.AppSettingsKeys.IVEncryptionKey)
                    _value = ConfigurationManager.AppSettings["IVEncryptionKey"];

                if (_AppSettingsKeys == AppConstant.AppSettingsKeys.PageSize)
                    _value = ConfigurationManager.AppSettings["PageSize"];

                return _value;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string CreatePath(string FullPath)
        {
            try
            {
                string ServerPath = System.Web.HttpContext.Current.Server.MapPath(FullPath);

                bool IsExists = System.IO.Directory.Exists(ServerPath);

                if (!IsExists)
                    System.IO.Directory.CreateDirectory(ServerPath);

                return ServerPath;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static string SaveFile(HttpPostedFileBase inputFile, AppConstant.FileType _FileType, string SpecificPath = "")
        {
            try
            {
                if (inputFile == null)
                    return null;

                string FileTypePath = "";

                if (_FileType == AppConstant.FileType.Image)
                    FileTypePath = "Images/";

                string FilePath = GetAppSettingsValueByKey(AppConstant.AppSettingsKeys.FileSystemPath) + FileTypePath + SpecificPath;
                string targetFolder = CreatePath(FilePath);

                string FileName = Guid.NewGuid().ToString() + new FileInfo(inputFile.FileName).Extension;

                string targetPath = Path.Combine(targetFolder, FileName);

                inputFile.SaveAs(targetPath);

                string DatabaseFilePath = "Images/" + SpecificPath + FileName;

                return DatabaseFilePath;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool DeleteFile(string FilePath)
        {
            bool IsFileDeleted = false;
            
            try
            {
                FilePath = GetAppSettingsValueByKey(AppConstant.AppSettingsKeys.FileSystemPath) + FilePath;
                string PhysicalPath = System.Web.HttpContext.Current.Server.MapPath(FilePath);
                        
                if ((System.IO.File.Exists(PhysicalPath)))
                    System.IO.File.Delete(PhysicalPath);
            }
            catch(Exception ex)
            {
                throw;
            }

            return IsFileDeleted;
        }
    }
}