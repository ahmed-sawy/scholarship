﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using App.Core.Helpers;
using App.UI.WEB.ASP.MVC.Base;

namespace App.UI.WEB.ASP.MVC.Base
{
    [CustomExceptionFilter]
    [CustomActionFilterAttribute]
    public class BaseController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            //string culture = "en-US";

            //if (Session[AppConstant.Lang] != null)
            //{
            //    if (Session[AppConstant.Lang].ToString() == CoreEnums.Lang.AR.ToString())
            //        culture = "ar-EG";

            //    WebAppHelper.Setcookie(AppConstant.Lang, Session[AppConstant.Lang].ToString());
            //}

            //CultureInfo CI = new CultureInfo(culture);
            ////CI.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            //Thread.CurrentThread.CurrentCulture = CI;
            //Thread.CurrentThread.CurrentUICulture = CI;

            //if (HttpContext != null)
            //{
            //    if (HttpContext.Request != null)
            //    {
            //        if (HttpContext.Request.IsAuthenticated)
            //            WebAppHelper.Setcookie(AppConstant.UserID, HttpContext.User.Identity.Name.ToString());
            //    }
            //}

            return base.BeginExecuteCore(callback, state);
        }
    }
}