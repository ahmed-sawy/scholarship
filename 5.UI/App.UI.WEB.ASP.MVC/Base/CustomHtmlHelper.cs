﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using App.Core.Helpers;
using App.Resources;

namespace App.UI.WEB.ASP.MVC.Base
{
    public static class CustomHtmlHelper
    {
        public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string Style = null, string DefaultValue = null)
        {
            if (Style == null)
                Style = "DatePicker form-control";

            if (DefaultValue == null)
                return helper.TextBoxFor(expression, "{0:" + CoreConst.dateFormat + "}", new { @class = "" + Style + " datefield", @onkeypress = "return false;", @autocomplete = "off" });
            else
                return helper.TextBoxFor(expression, "{0:" + CoreConst.dateFormat + "}", new { @class = "" + Style + " datefield", @Value = DefaultValue, @onkeypress = "return false;", @autocomplete = "off" });
        }
    }
}