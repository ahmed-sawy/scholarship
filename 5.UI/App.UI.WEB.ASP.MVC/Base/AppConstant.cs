﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Base
{
    public static class AppConstant
    {
        public const string ApplicationCookie = "ApplicationCookie";
        public const string Lang = "Lang";
        public const string UserID = "UserID";


        public const string T_CategoryLst = "T_CategoryLst";
        public const string T_MenuLanguageLst = "T_MenuLanguageLst";


        public enum UserPorfile
        {
            ID,
            FirstName,
            LastName,
            Email,
            UserType
        }

        public enum AppSettingsKeys
        {
            FileSystemPath,
            EncryptionKey,
            PageSize,
            IVEncryptionKey
        }

        public enum FileType
        {
            Image
        }
    }
}