﻿using App.Core.Helpers;
using App.Domain.Model;
using App.UI.WEB.ASP.MVC.IBase;
using App.UI.WEB.ASP.MVC.Base;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using App.Framework.Business.ILogic;

namespace App.UI.WEB.ASP.MVC.Base
{
    public class AuthenticationUser : IAuthenticationUser
    {
        private readonly IUserLogic _UserLogic;

        public AuthenticationUser(IUserLogic _UserLogic)
        {
            this._UserLogic = _UserLogic;
        }

        public async Task<CoreEnums.SignInStatus> SignIn(string UserName, string password)
        {
            try
            {
                password = CoreHelper.Encrypt(password, WebAppHelper.GetAppSettingsValueByKey(AppConstant.AppSettingsKeys.EncryptionKey), WebAppHelper.GetAppSettingsValueByKey(AppConstant.AppSettingsKeys.IVEncryptionKey));

                User _RegisterUser = (await this._UserLogic.GetUser(x => x.Email == UserName && x.Password == password)).FirstOrDefault();

                if (_RegisterUser == null)
                    return CoreEnums.SignInStatus.UserNotFound;


                List<Claim> claims = new List<Claim>();

                claims.Add(new Claim(ClaimTypes.Name, _RegisterUser.ID.ToString()));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, _RegisterUser.FirstName));
                claims.Add(new Claim(ClaimTypes.Role, _RegisterUser.UserType.ToString()));

                ClaimsIdentity _ClaimsIdentity = new ClaimsIdentity(claims, AppConstant.ApplicationCookie);

                if (HttpContext.Current.Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
                    this.SignOut();

                HttpContext.Current.Request.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, _ClaimsIdentity);

                HttpContext.Current.User = new GenericPrincipal(_ClaimsIdentity, null);

                return CoreEnums.SignInStatus.Success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SignOut()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                HttpContext.Current.Request.GetOwinContext()
                           .Authentication
                           .SignOut(HttpContext.Current.Request.GetOwinContext()
                           .Authentication.GetAuthenticationTypes()
                           .Select(o => o.AuthenticationType).ToArray());

                HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            }
        }

        public object GetUserProfile(AppConstant.UserPorfile UserPorfileProperty)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
                return null;

            if (UserPorfileProperty == AppConstant.UserPorfile.ID)
                return HttpContext.Current.Request.GetOwinContext().Authentication.User.FindFirst(ClaimTypes.Name).Value;

            else if (UserPorfileProperty == AppConstant.UserPorfile.FirstName)
                return HttpContext.Current.Request.GetOwinContext().Authentication.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            else if (UserPorfileProperty == AppConstant.UserPorfile.UserType)
                return HttpContext.Current.Request.GetOwinContext().Authentication.User.FindFirst(ClaimTypes.Role).Value;

            return null;
        }
    }
}