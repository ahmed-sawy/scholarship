﻿using log4net;
using System;
using System.Web.Mvc;
using App.Core.Log;
using App.UI.WEB.ASP.MVC.IBase;

namespace App.UI.WEB.ASP.MVC.Base
{
    public class CustomExceptionFilter : HandleErrorAttribute
    {
        private static readonly ILog AppLogger = AppLogHelper.GetLogger(typeof(CustomExceptionFilter));
        private readonly IAuthenticationUser _AuthenticationUser;

        public CustomExceptionFilter()
        {
            _AuthenticationUser = DependencyResolver.Current.GetService<IAuthenticationUser>();
        }

        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            if (filterContext.ExceptionHandled || filterContext.HttpContext.IsCustomErrorEnabled)
                return;

            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            string LogMessage = string.Empty;
            string Area = string.Empty;

            if (filterContext.RouteData.DataTokens["area"] != null)
                Area = filterContext.RouteData.DataTokens["area"].ToString();

            string Controller = filterContext.RouteData.Values["controller"].ToString();
            string Action = filterContext.RouteData.Values["action"].ToString();

            string UserName = string.Empty;

            if (filterContext.HttpContext.Request.IsAuthenticated)
                UserName = _AuthenticationUser.GetUserProfile(AppConstant.UserPorfile.FirstName).ToString();

            LogMessage = string.Format("[User: {0}] [Area: {1}] [Controller: {2}] [Action: {3}]", UserName, Area, Controller, Action);

            AppLogger.Error(LogMessage, ex);

            filterContext.Result = new ViewResult()
            {
                ViewName = "~/Views/Shared/Error.cshtml",
            };
        }
    }
}