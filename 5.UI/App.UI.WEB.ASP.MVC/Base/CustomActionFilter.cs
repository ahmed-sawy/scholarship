﻿using log4net;
using System;
using System.Web.Mvc;
using App.Core.Log;
using App.UI.WEB.ASP.MVC.Base;
using App.UI.WEB.ASP.MVC.IBase;

namespace App.UI.WEB.ASP.MVC.Base
{
    public class CustomActionFilterAttribute : ActionFilterAttribute
    {
        private static readonly ILog AppLogger = AppLogHelper.GetLogger(typeof(CustomActionFilterAttribute));
        private readonly IAuthenticationUser _AuthenticationUser;

        public CustomActionFilterAttribute()
        {
            _AuthenticationUser = DependencyResolver.Current.GetService<IAuthenticationUser>();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            try
            {
                base.OnResultExecuted(filterContext);

                string UserName = "";

                if (filterContext.HttpContext.Request.IsAuthenticated)
                    UserName = _AuthenticationUser.GetUserProfile(AppConstant.UserPorfile.FirstName).ToString();

                if (string.IsNullOrEmpty(UserName))
                    return;

                string Area = string.Empty;
                string LogMessage = string.Empty;

                string Controller = filterContext.RouteData.Values["controller"].ToString();
                string Action = filterContext.RouteData.Values["action"].ToString();

                if (filterContext.RouteData.DataTokens["area"] != null)
                    Area = filterContext.RouteData.DataTokens["area"].ToString();

                if (string.IsNullOrEmpty(Area))
                    LogMessage = string.Format("[User: {0}] [Controller: {1}] [Action: {2}]", UserName, Controller, Action);
                else
                    LogMessage = string.Format("[User: {0}] [Modul: {1}] [Controller: {2}] [Action: {3}]", UserName, Area, Controller, Action);

                AppLogger.Info(LogMessage);
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}