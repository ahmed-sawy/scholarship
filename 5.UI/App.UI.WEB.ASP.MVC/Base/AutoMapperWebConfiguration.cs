using App.Domain.Model;
using App.UI.WEB.ASP.MVC.Models;
using AutoMapper;

namespace App.UI.WEB.ASP.MVC.Base
{
    public class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Scholarship, ScholarshipModel>().ReverseMap();
            });
        }
    }
}
