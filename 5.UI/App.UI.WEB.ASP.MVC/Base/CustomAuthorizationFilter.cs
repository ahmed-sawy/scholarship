﻿using App.UI.WEB.ASP.MVC.Base;
using App.UI.WEB.ASP.MVC.Helpers;
using App.UI.WEB.ASP.MVC.IBase;
using System.Web.Mvc;
using System.Web.Routing;

namespace App.UI.WEB.ASP.MVC.Base
{
    public class CustomAuthorizationFilter : AuthorizeAttribute
    {
        private readonly IAuthenticationUser _AuthenticationUser;
        private readonly string[] allowedroles;

        public CustomAuthorizationFilter(params string[] roles)
        {
            _AuthenticationUser = DependencyResolver.Current.GetService<IAuthenticationUser>();
            allowedroles = roles;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (AuthorizeRequest(filterContext))
                return;

            HandleUnauthorizedRequest(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);

            //Code to handle unauthorized request
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Area", null }, { "action", "Index" }, { "controller", "Login" } });
        }

        private bool AuthorizeRequest(AuthorizationContext filterContext)
        {
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
                return true;

            //string Area = string.Empty;

            //if (filterContext.RouteData.DataTokens["area"] != null)
            //    Area = filterContext.RouteData.DataTokens["area"].ToString();

            //string _ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            //string _ActionName = filterContext.ActionDescriptor.ActionName;

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                byte AuthenticationRoleID = 0;
                byte.TryParse(_AuthenticationUser.GetUserProfile(AppConstant.UserPorfile.UserType).ToString(), out AuthenticationRoleID);

                foreach (var role in allowedroles)
                {
                    if (AuthenticationRoleID == (byte)SecurityEnums.Role.Admin)
                        return true;

                    if (AuthenticationRoleID == (byte)SecurityEnums.Role.Graduate)
                        return true;
                }

                return false;
            }

            return false;
        }
    }
}