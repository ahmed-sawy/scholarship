﻿using App.Framework.Business.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Models
{
    public class ScholarshipListModel
    {
        public List<ScholarshipModel> Scholerships { get; set; }
        public ScholershipFilters ScholershipFilters { get; set; }
    }
}