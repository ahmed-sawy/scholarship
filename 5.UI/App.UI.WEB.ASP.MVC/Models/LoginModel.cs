﻿using App.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Models
{
    public class LoginModel
    {
        [MaxLength(500)]
        [Display(Name = "UserName", ResourceType = typeof(AppResources))]
        [Required(ErrorMessageResourceType = typeof(AppResources), ErrorMessageResourceName = "UserNameRequired")]
        public string username { get; set; }

        [MaxLength(50)]
        [Display(Name = "Password", ResourceType = typeof(AppResources))]
        [Required(ErrorMessageResourceType = typeof(AppResources), ErrorMessageResourceName = "PasswordRequired")]
        public string pass { get; set; }
    }
}