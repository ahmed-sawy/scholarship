﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Models
{
    public class UserModel
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
        public int UserType { get; set; }
    }
}