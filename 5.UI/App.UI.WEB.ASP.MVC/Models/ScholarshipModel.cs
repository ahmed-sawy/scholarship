﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.UI.WEB.ASP.MVC.Models
{
    public class ScholarshipModel
    {
        public long ID { get; set; }
        public string NationalID { get; set; }
        public string University { get; set; }
        public string Major { get; set; }
        public decimal GPA { get; set; }
        public string Resume { get; set; }
        public long UserID { get; set; }
        public bool IsApproved { get; set; }
    }
}