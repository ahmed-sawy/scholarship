﻿$(document).ajaxStart(function () {
    $('#spinnerDiv').show();
});

$(document).ajaxComplete(function () {
    $.validator.unobtrusive.parse(document);
    $('#spinnerDiv').hide();
    $('.DatePicker').datepicker({
        dateFormat: "dd/mm/yy",
    });
    $(".ListBtnActionOnClick").click(function (e) {
        $('.HiddenActionName').val(e.target.value);
    });
});

function FadeOutText(input) {
    jQuery('.' + input).delay(2000).fadeOut('slow');
}

$(document).ready(function () {
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    $('.DatePicker').datepicker({
        dateFormat: "dd/mm/yy",
    });
});

$(".ListBtnActionOnClick").click(function (e) {
    $('.HiddenActionName').val(e.target.value); 
});

$(document).on("keydown", ".NumberOnly", function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


function displayImage(input, imageId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var filename = $('#' + input.name).val();

        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        reader.onload = function (e) {

            $('#' + imageId).attr('src', e.target.result);
            $('#' + imageId).hide();
            $('#' + imageId).fadeIn(500);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
